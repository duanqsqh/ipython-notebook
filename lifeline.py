from util import ttr
from qsutil.gspace import g
from qsutil.u import ucheck
from study.backtest2104 import BaseStrategy, get_index_eod

# from study.tlog import Tlog
# from study.trade_chart_hilo import CandlePlotter

if not hasattr(g, 'df'):
    g.df = get_index_eod(g.ticker)
g.start, g.end= '2020-1-1', '2020-12-31'; 
g.n_sma=5

#%% script

from study.backtest2104 import BaseStrategy, get_index_eod
class LifelineStrat(BaseStrategy):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


self = LifelineStrat(df=g.df, code=g.ticker, start=g.start, end=g.end, n_sma=g.n_sma)
self.load()
self.ind()
self.plotter._candles()
self.plotter._plot_trend()     

self.get_signal(0)
self.show_event(bsig='jc', ssig='sc')    
# self.simplest_bt(print_log=False, )
# self.simplest_bt(print_log=False,  bprice_ref='lline')
# self.simplest_bt(print_log=False, bprice_ref='lline', sprice_ref='lline')
# self.simplest_bt(print_log=0, bprice_ref='lline', sprice_ref='lline')
self.simplest_bt(print_log=1, bprice_ref='lline', sprice_ref='lline')


print(f"\n\n策略参数n_sma={self.p['n_sma']}")
print('\n策略绩效-------')
ttr.perf_measure(self.df.equity, with_print=True)
print('\nBnH绩效-------')
ttr.perf_measure(self.df.close, with_print=True)
 
'''
# self.mak
# ucheck.getmembers(g)
# ucheck.getmembers(self)
print(); print(self.p)
self.make_subplot(plot_signal=1)
self.make_subplot(plot_signal=1, plot_equity=1)
'''
self.make_subplot(plot_signal=1, plot_equity=1)

# Here is the main entry:
# self = utest(5)    
# for n in range(3,15, 1): self= utest(n)       
# self= utest(cook=False)       
  
'''
In [64]: runfile('D:/algolab/study/utest/lifeline.py', wdir='D:/algolab/study/utest')
Reloaded modules: 
    util, pyh, pyh.pyh, 
    toolkit, toolkit.pf, toolkit.myDataIO, 
    util.jqds, util.ttr, 
    qsutil, qsutil.u, qsutil.u.ucheck, 
    study, dqsbt, dqsbt.util2, 
    qsutil.pkl, qsutil.gspace, qsutil.impmodule, qsutil.db, 
    util.tdxhq, 
    packages, packages.report, 
    qsutil.db.dboperation, dqsbt.util2.util, 
    candleplotter, study.tlog, qsutil.u.uload, 
    study.backtest2104, qsutil.u.uplot  
    
In [66]: runcell('script', 'D:/algolab/study/utest/lifeline.py')
'''
    
    
