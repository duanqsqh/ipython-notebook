# coding: utf-8
# tt.py


#~ import sys
#~ reload(sys) 
#~ sys.setdefaultencoding( "utf-8" ) 

a=True
seperator = "======  seperator line  ========"
#~ seperator = "======  分割线  ========"
#~ seperator = u"======  分割线  ========"  # 这样就会出错了???

# UnicodeEncodeError: 'ascii' codec can't encode characters in position 8-10: ordinal not in range(128)
# 在cmd窗口里, 运行python tt.py程序，能正常显示结果
# 但是, 在sciTE的环境里, F5却出现乱码, why???, 结果如下所示.
#~ a
#~ ======  seperator of line  ========
#~ ======  鍒嗗壊绾? ========
#~ a
#~ c

# 经过上述1), 2)的设置后, 终于在输出窗口里显示正常的中文了. 如下所示:
#~ a
#~ ======  分割线  ========
#~ a
#~ c



def test():
    u"""
    看看python语言对缩进的要求是多么的严格
    下面的test()和test1()两个函数貌似一样, 其实不然. 
    test1()里因为混用了缩进方式, 其'print c'语句已经不属于else条件下的语句了. 
    
    运行的结果为:
    a
    ======  seperator of line  ========
    a
    c
    
    注意:   你需要打开编辑器的 查看/选中空格  以便使空格和tab能显示出来
                一般地, 空格显示为点号, tab显示为右箭头.
                
    为了避免因代码缩进而产生不必要的麻烦，写python代码应该，
    使用唯一的缩进方式（要么tab，要么空格），使用固定和统一的编辑器，
    此外，还应该利用好编辑器的一些特性。
    
    做不到的话, 至少要在同一个代码块里做到使用唯一的缩进方式. 
    用空格的好处: 
    空格，好处是任何人用任何编辑器查看代码都是对齐的，包括网页上查看（比如在GitHub上看代码）。
    很多用tab的代码，在网页上查看时对齐就乱了。
    用tab的好处和害处:
    省事, 一键顶4键用
     Tab 无法做到行内 行末代码或注释的对齐,而空格啥都可以
     
     另外一种说法:
     作者：pansz
    链接：https://www.zhihu.com/question/19960028/answer/15262434
    来源：知乎
    著作权归作者所有，转载请联系作者获得授权。

    使用空格还是 tab 的这个问题，如同程序员之间的『语言之争』，是个永远的圣战，这个争论不会有结果，
    你怎么选择都有自己的道理，只是看你选择认同谁而已。

    就我而言，我提倡尽可能用空格（除了少数必须用tab的情形以外）。理由如下：

    空格在各种情况下代码都是你想要的样子。而 tab 仅仅当你与代码作者的 tab 尺寸设定为相同时，代码才好看。
    修改 tab 尺寸并不能解决这个问题，因为你很难做到每打开一个文件就修改一次 tab 尺寸，
    而每个人通常有不同的习惯（POSIX/Unix 标准的 tab 应当为 8 字符宽度，
    Linus 大神也规定 Linux 内核中所有代码的 tab 尺寸为 8）。
    如果存在行尾注释，则 tab 尺寸更加是必须设定为与作者相同，这就意味着你看不同的代码需要经常修改 tab 尺寸。
    我看过许多代码，其使用的 tab 尺寸有从 2,3,4,5,6,8,16 甚至 32 的，如果你使用的 tab 尺寸与作者不同，外观将很不理想。
    靠谱的编辑器都能解决前进后退增加减少缩进的问题，即便是四个空格，一个退格键也能全退了，
    所以在使用的方便性方面根本不存在问题。——如果抱怨删除调整还不能有效解决的，你需要研究一下你的编辑器了。

    实际上增加减少缩进在主流编辑器中都直接有快捷键，无论是 tab 还是空格还是退格都很少直接被用于缩进。
    tab 是制表符而不是缩进符，正如在 html 页面中大量使用<table>进行布局是个不好的编程习惯一样，
    在编程中大量使用制表符布局通常也不是个好习惯。
    如果代码需要压缩发布，使用空格的代码通常具有更好的压缩率。
    各位不信的可以使用批处理工具把代码用全空格或者全 tab 走一遍。
    ——这里面的原理是信息量，使用 tab 缩进的代码中，仍然不可避免的含有空格（运算符之间的间隔，注释等等），
    但使用空格的代码中根本不含有 tab，这使得 tab 缩进代码虽然不压缩的时候更小，
    但熵更高，因而压缩率较差，压缩之后反而更大。
    ——当然，压缩发布代码仅仅对开源软件有意义，商业软件可以无视。

    QQQ: 如何更改scite的缩进设置, 让tab自动转换为4个空格?
    终于挖到答案了.
    AAA: scite的选项==>更改缩进设置==>弹出''更改设置"小窗口
                在这个窗口里: 有个使用tabs单选按钮, 默认状态是勾选的, 就是说默认地情况下是选择了使用tabs而不是4个空格
                所以把这个勾选去掉, 点击确定, 关闭小窗口
                大功告成!!! 
                亲测是可以的了.
    
    Autoit编辑器如何把TAB转换成空格[已解决] - 已解决问题区 - 『 提问交流 』 - AUTOIT CN AutoIt中文论坛|acn|au3|软件汉化 - 分享您的技术!  
    http://www.autoitx.com/thread-40253-2-1.html


    """
    a=True
    if a:
        print 'a'
    else:
        print 'a'
        print 'c'


def test1():
    a=True
    if a:
        print 'a'
    else:
        print 'a'
        print 'c'  # 这里混用了空格和tab作为缩进符, 已经被sciPE for python检查出来了(蓝色的小三角)

#~ test1()

if __name__== '__main__':
    test()
    #~ print  "======  seperator of line  ========"
    print  seperator
    test1()
    



# 'ascii' codec can't encode characters 中文字符编码的问题，完美解决办法
# 1. 在python脚本的前面加三句话： 
#      import sys
#      reload(sys) 
#      sys.setdefaultencoding( "utf-8" ) 
# 2. 如果是在scite环境里执行python脚本的话, 还需要把UserHome目录下的SciTEUser.properties
#      配置文件里的output.code.page设置为65001 (表示输出窗口的编码方式为utf-8), 否则中文依旧是乱码
