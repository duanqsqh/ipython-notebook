
import datetime

import numpy as np, pandas as pd
import matplotlib as mpl# 个性配置文件: C:\Users\Administrator\.matplotlib\matplotlibrc
import matplotlib.pyplot as plt 
from matplotlib.ticker import FuncFormatter, MaxNLocator # 刻度的格式, 刻度的定位
import mplfinance as mpf; _=mpf
from mplfinance.original_flavor import candlestick_ohlc
# from cycler import cycler# 用于定制线条颜色

from util import ttr
from qsutil.u import uplot


#%%

class CandlePlotter:
    
    cweekday={
        '1':'一',
        '2':'二',
        '3':'三',
        '4':'四',
        '5':'五',
    }
    
    def __init__(self, df=None, p=None):
        self.df=df
        self.p=p
        


  
    ### plot method ---------------      
    def _candles(self, ax=None, nrows=2):
        '''绘制k线图 
        df : df with ohlcv 
        
                
        平均k线指标： Heikin Ashi
        HAClose= (Open+High+Low+Close)/4
                (The average price of the current bar)
        HAOpen= (Open of Prev. Bar+Close of Prev. Bar) /2
                (The midpoint of the previous bar)
        HAHigh=Max[High, HAOpen, HAClose]
        HALow=Min[Low,   HAOpen, HAClose]
        ​	        
                
        '''
        
        if ax is None:
            if nrows==2:
                self.fig, (self.ax, self.ax2) = \
                    plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [4,1]}, figsize=self.p['figsize'] )
                    # plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [3,1]} )
            elif nrows==3:
                self.fig, (self.ax, self.ax2, self.ax3) = \
                    plt.subplots(3, 1, sharex=True, gridspec_kw={'height_ratios': [3,1,2]} )        
        if mpl.rcParams['backend']=='Qt5Agg':
            figw, figh = len(self.df)/10, 6
        elif mpl.rcParams['backend']=='module://ipykernel.pylab.backend_inline':
            figw, figh = len(self.df)/10*2, 6*2
        self.fig.set_size_inches(figw, figh) # 10表示每英寸宽度内画出10根k线

        
        
        df=ohlc=self.df
        x=np.arange(len(df)); 
        length = len(df)
        if length>190:
            print(f'\n\n数据长度={length}, 超过了190, k线图可能显得较拥挤. 3个季度的日线长度较为合适.')
        # x=plt.matplotlib.dates.date2num(df.index)
        
        # quotes = zip(x, 
        #       df.iloc[:, 0], # ohlc['O'], 打包可迭代变量到元组
        #       df.iloc[:, 1],
        #       df.iloc[:, 2],
        #       df.iloc[:, 3],
        #       )
        # width=0.7
        # lines, rects = candlestick_ohlc(self.ax, quotes, width,
        #                  colorup= '#db3f3f', #     'w',   # 'r',  #'#77d879', 
        #                  colordown='#77d879', # 'w', # g',  #'#db3f3f'
        #                  alpha=0.5, 
        #                  )
        
        
        def default_col_func(index, open1, close, low, high):
            return 'black' if open1[index] > close[index] else 'white' # r g b  cyan black white
        open1,high,low,close = ohlc.open, ohlc.high, ohlc.low, ohlc.close        
        candle_colors = [default_col_func(i, open1, close, low, high) for i in x]
        # 计算出 每日的开盘价/收盘价里的最大值和最小值
        oc_min = pd.concat([open1, close], axis=1).min(axis=1)
        oc_max = pd.concat([open1, close], axis=1).max(axis=1)
        candles = self.ax.bar(x, oc_max-oc_min, bottom=oc_min, color=candle_colors, linewidth=1.0, edgecolor='black', align='center', alpha=0.5)
        # candles = self.ax.bar(x-0.4, oc_max-oc_min, bottom=oc_min, color=candle_colors, linewidth=0.2, edgecolor='black')
        shadlines_up = self.ax.vlines(x, oc_max, high, color=['black']* length, linewidth=0.6)
        shadlines_dn = self.ax.vlines(x, low, oc_min,  color=['black']* length, linewidth=0.6)
        _,_,_=candles, shadlines_dn, shadlines_up
        #        
        
        deltat = (df.index[20] - df.index[0])
        freq = "Eod" if deltat > datetime.timedelta(days=20) else 'Intra'
        fmt_dt = '%H:%M(%V.%w)\n%y.%m.%d' if freq=='Intra' else '%m %d\n%Y'
        # fmt_dt = '%H:%M\n%m %d\n%Y' if freq=='Intra' else '%m %d\n%Y'
        if freq=='Intra':
            myLabels_ = list(df.index.strftime(fmt_dt))
            myLabels = list(map(lambda a: ''.join((
                a[:9], self.cweekday[a[9]], a[10:])), myLabels_))
        else: 
            myLabels = list(df.index.strftime(fmt_dt))
        def format_fn(tick_val, tick_pos):
            "格式化函数, 把x轴上的刻度定位标记 从序号转换为对应的时间/日期 "
            # print((tick_val))
            if int(tick_val) in x:
                return myLabels[int(tick_val)]
            else:
                return ''
                
        self.ax.xaxis.set_major_locator(MaxNLocator(integer=True))        
        self.ax.xaxis.set_major_formatter(FuncFormatter(format_fn))

        self._vol_bars()
    
        self._suptitle()
      
    def _vol_bars(self):
        ohlc=self.df
        open1,high,low,close = ohlc.open, ohlc.high, ohlc.low, ohlc.close
        volume = ohlc['volume']
        
        color='k'
        def col_func(index, open1, close, low, high):
            return color if open1[index] > close[index] else 'white' # r g b  cyan black white
        
        x = np.arange(len(close))
        bar_colors = [col_func(i, open1, close, low, high) for i in x]
        
        
        volume_scale = None
        scaled_volume = volume
        if volume.max() > 1000*1000:
            volume_scale = u'百万股' #'M'
            scaled_volume = volume / 1000.0/1000.0
        elif volume.max() > 1000:
            volume_scale = u'千股'
            scaled_volume = volume / 1000.0
        #self.ax2.bar(x-0.4, scaled_volume, color=self.candle_colors, linewidth=0.6, edgecolor='black')
        self.ax2.bar(x, scaled_volume, 0.85, align='center',
                     color=bar_colors, 
                     linewidth=0.6, 
                     edgecolor=[color]*len(volume))
        volume_title = 'Volume'
        if volume_scale:
            volume_title = 'Volume (%s)' % volume_scale
        #ax2.set_title(volume_title) # 太难看了
        self.ax2.set_ylabel(volume_title, fontdict=None)
        # self.ax2.xaxis.grid(False)      

    def _suptitle(self, title='By:btest2104+lifeline策略', fsize=10):
        dt1 = self.df.index[0].strftime('%Y.%m.%d')
        dt2 = self.df.index[-1].strftime('%Y.%m.%d')
        t = f'{self.p["dname"]}({self.p["code"]}), ' + \
            f'时间段: [{dt1}, {dt2}], ' + \
            f'时间框/时间颗粒: {self.p["freq"]}\n' + \
            f'{title}'
            # f'时间段: [{self.p["start"]}, {self.p["end"]}], ' + \
        self.fig.suptitle(t, fontsize=fsize)
        self.title = t
        
    def _plot_trend(self, plot_fast_slow_ma=False, plot_3jx=False, plot_mm=False, plot_sl=False, ):
        
        if plot_fast_slow_ma:
            self.df.mafast.plot(ax=self.ax, use_index=False, legend=True, label=f'mafast:{self.df.mafast[-1]:.2f}')
            self.df.maslow.plot(ax=self.ax, use_index=False, legend=True, label=f'maslow:{self.df.maslow[-1]:.2f}')
            
        if plot_mm: # moving median of high and low price
            self.df['mml'].plot(ax=self.ax, use_index=False, label='mml', linewidth=0.75)
            self.df['mmh'].plot(ax=self.ax, use_index=False, label='mmh', linewidth=0.75)
        
        if plot_sl:
            self.df['sl'].plot(ax=self.ax, use_index=False, label='sl',marker='o', markersize=4)
        
        if plot_3jx:
            if 'sma3' in self.df.columns:
                self.df['sma3'].plot(ax=self.ax, use_index=False, label='sma3', color='red')
            
            if 'mama' in self.df.columns:
                self.df['mama'].plot(ax=self.ax, use_index=False, label='mama', color='k', )
            
            if 'sma' in self.df.columns:
                self.df['sma'].plot(ax=self.ax, use_index=False, label='sma')

        # if 'trend' in self.df.columns:
        if 0: #'trend' in self.df.columns:
            # self.df['trend'].plot(ax=self.ax, use_index=False, label='trend', linewidth=1)
            self.df['ema'].plot(ax=self.ax, use_index=False, label='ema', linewidth=1)
            self.df['wh'].plot(ax=self.ax, use_index=False, label='wh', linewidth=1)
            self.df['wt'].plot(ax=self.ax, use_index=False, label='wt', linewidth=1)
            self.df['wl'].plot(ax=self.ax, use_index=False, label='wl', linewidth=1)
            self.df['outa'].plot(ax=self.ax, use_index=False, label='outa', linewidth=1, marker='o', markersize=2)
            # self.df['c0'].plot(ax=self.ax, use_index=False, label='c0', linewidth=1, marker='o', markersize=2)
            # tmp = self.df.trend + self.df.atr20
            # tmp.plot(ax=self.ax, use_index=False, label='trend+atr20')
            # tmp = self.df.trend + self.df.tr
            # tmp.plot(ax=self.ax, use_index=False, label='trend+tr')

        # if 'mid' in self.df.columns:
            # self.df.mid.plot(ax=self.ax, use_index=False,  color='k', linewidth=.6)

        if 'hhv' in self.df.columns:
            self.df['hhv'].plot(ax=self.ax, use_index=False, label='hhv', color='k', linewidth=.6)
        if 'llv' in self.df.columns:
            self.df['llv'].plot(ax=self.ax, use_index=False, label='llv', color='k', linewidth=.6)
            self.df['lline'].plot(ax=self.ax, use_index=False, label='lifeline', color='red', linewidth=.6)
        
            # nmc = self.df['nmoney_cum']
            # _ = nmc/nmc[0] * self.df.close[0]
            # _.plot(ax=self.ax, use_index=False, label='nmoney_cum', color='green', linewidth=.6)
 
        
        self.ax.legend()
        self.custom_fig()
        
        


    def _plot_buysignal(self, sig_type='truebuy', label='cdft', color='k'):    
        df, ax=self.df, self.ax
        truebuy = getattr(self, sig_type)
        
        x=truebuy.index # index of signal location
        x= [df.index.get_loc(x) for x in truebuy.index]  # seq number
        buy_count = truebuy.index.size
        ax.vlines(x, df.low[x]/1.05, df.low[x]/1.01, 
                  color=color, lw=1.8,
                  label='bsig_{} ({:d})'.format(label, buy_count, ))
        ax.legend()

    def _plot_signal(self, bsig=None, ssig=None):
        uplot.vlines (self.ax, getattr(self.df, bsig), self.df.llv, 'green', position='below')
        uplot.vlines (self.ax, getattr(self.df, ssig), self.df.hhv, 'red', position='above')
        

    def _plot_box_btn_cross(self):
        df=self.df
        uc = ttr.upCross(self.df.sma, self.df.trend_w)
        dc = ttr.downCross(self.df.sma, self.df.trend_w)
        uc_x = list(map(df.index.get_loc, uc[uc==True].index))
        dc_x = list(map(df.index.get_loc, dc[dc==True].index))
        cross_x = dc_x + uc_x
        cross_x.sort()
        
        x = cross_x
        width = [xx - x[i] for i,xx in enumerate(x[1:]) ]
        width.append(len(df) - x[-1])
        
        bot = list(map(df.trend_w.__getitem__, x))
        bot = [v*1.0      for v in bot]
        hei = [v*.02 for v in bot]
        self.ax.bar(x, hei, width=width, 
                   align='edge',  #'center', 
                   bottom=bot,
                   color='white', 
                   linewidth=0.6, 
                   edgecolor='k', alpha=0.5
                   )

    def _plot_equity(self):        
        self.df['equity'].plot(
            ax=self.ax, use_index=False, 
            color='m', marker='o', markersize=4,
            label='equity({:.2f}) vs bm({:.2f})'.format(
                self.df.equity[-1]/self.df.equity[0],
                self.df.close[-1]/self.df.close[0],
                ),
            legend=True,
        )
    

    def plot_tsl(self):
        ax, df, sl_l = self.ax, self.df, self.tlog.sl_l
        horizon = self.tlog.horizon
        
        index = map(lambda x: x[0], sl_l)
        x = list(map(df.index.get_loc, index))
        y = list(map(lambda x: x[1], sl_l))
        ax.plot(x, y, 'r+', label='tsl')
        
        
        index = map(lambda x: x[0], horizon)
        x = list(map(df.index.get_loc, index))
        y = list(map(lambda x: x[1], horizon))
        ax.plot(x, y, 'bx', label='horizon',markersize=6) # ^ x X * 
        # ax.plot(x, y, 'b_', label='horizon',markersize=6) # ^ x X * 
        # ax.plot(x, y, 'b\bigwedge', label='horizon',markersize=6) #^
        # 'plot formatter: [color][marker][line-style]'
        
        
        if 'equity' in self.df.columns:
            self._plot_equity()
            
        freq,code=self.p['freq'], '沪深300指数'
        c=self.df.close; fmt = '%Y%m%d'
        text = '{}, 颗粒:"{}", 时间段: {} -- {} '.format(code,freq, c.index[0].strftime(fmt), c.index[-1].strftime(fmt))
        par_tpl = tuple(map(lambda x: (x, self.p.get(x)), self.mparams))
        text += f'\n参数取值: {par_tpl}'
        # .format(self.p['moveup_level'], self.p['n_tsl'])
        pre_title = '趋势跟随 + 移动止损策略 '
        title = pre_title + '蜡烛图和策略权益'
        self.fig.suptitle('\n'.join([title, text]), fontsize=8)
        self._beautify_xTickLabel(0 )

        ax.legend()
      
        
        

    #### plot 方法之辅助方法
    def _beautify_xTickLabel(self, rotation=45, ha='center', fs=8):
        for xtl in self.ax.get_xticklabels():
            #print xtl.get_rotation(), xtl.get_ha()
            xtl.set_rotation(rotation) # 设置为水平走向
            xtl.set_ha(ha)
            xtl.set_fontsize(fs)
        self._subplots_adj()
        
    def _subplots_adj(self):
        self.fig.subplots_adjust(wspace=0.01, hspace=0.01, 
                bottom=0.09, top=0.95, 
                left=0.06, right=0.95)
        
        
    def custom_fig(self):
        self.ax.set_yscale(value='log', base=1.1) # 1.1, 2, 10
        self.ax.get_yaxis().grid(True, 'major', color='0.1', linestyle='dotted', lw=0.3)
        self.ax.get_yaxis().grid(True, 'minor', color='0.3', linestyle='dotted', lw=0.1)
        self.ax.get_xaxis().grid(True, 'major', color='0.3', linestyle='dotted', lw=0.3)
        self._subplots_adj()
        



#%% shit

   