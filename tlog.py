class Tlog:

    def __init__(self, p):
        self.p = p # param dict from CandlePlotter
        
        self.pos, self.pos_l = 0, []
        self.sl, self.sl_l, self.horizon = 0, [], []

        self.bprice, self.hline, self.nbar = None, None, None
        self.sroc1, self.sroc1_l = 0, [] 
        self.comm = 1/100/100.
        # 策略的当日收益, 历史收益, 买入日的为零-佣金, 
        # 卖出日的是止损价相对于前一日的收盘价的收益率-佣金
        
        self.bdt = None
        self.ssn, self.srecord = 0, [] # sell_seq_num, sell_record_history
        self.srecord_title = 'ssn sbar sdt sprice bbar bdt bprice nbar gross_pnl'.split()
        self.log_title= '\n操作指令____ i___ 日期_____ pos nbar price  stoploss sroc1 平仓交易收益率'
        
    def buy(self, i, dt, row, df, sl):
        self.bprice = row.close; self.bdt=dt; self.bbar=i;
        self.pos=1; 
        self.pos_l.append((dt, self.pos))
        self.nbar=0
        
        # self.sl = df.low[i-3:i+1].min(); 
        self.sl = sl #row.llv 
        self.sl_l.append((dt, self.sl)) #4根内的低点 记为止损
        
        # self.hline = self.bprice*(1+self.p['moveup_level']/100)
        self.hline = self.bprice
        self.horizon.append((dt, self.hline))
        
        sroc1 = -self.comm
        self.sroc1_l.append((dt, sroc1))
        print(' '.join([
            f'{"BuyIt":<10s}|', 
            f'{i:>4d}',
            f'{dt.date().isoformat()}',
            
            f'{self.pos:>2d}',
            f'{self.nbar:>2d}',

            f'{self.bprice:.2f}',
            f'{self.sl:.2f}',
            f'{sroc1:>7.2%}',
            ]))
        return self.pos

    def sell(self, i, dt, row, df, sprice_ref='last_sl'):
        self.pos=0; 
        self.pos_l.append((dt, self.pos))
        self.horizon.append((dt, self.hline))
        
        if sprice_ref=='last_sl':
            sprice = self.sl
            # print( i,dt, f'sprice:{sprice:.2f}')
        elif sprice_ref=='curr_close':
            sprice = row.close
        elif sprice_ref=='row_ema':
            sprice = row.ema
        else:
            sprice = min(self.sl, row.open) # 考虑向下的跳空缺口
        sroc1 = sprice/row.close1 - 1 - self.comm
        self.sroc1_l.append((dt, sroc1))
        self.nbar += 1

        self.ssn += 1
        # self.srecord_title = 'ssn sbar sdt sprice bbar bdt bprice nbar gross_pnl'.split()
        closed_pnl = (sprice/self.bprice - 1)*100
        _ = (self.ssn, i,dt,sprice,         
             self.bbar,self.bdt, self.bprice, 
             self.nbar, closed_pnl,)
        self.srecord.append(_)
        print(' '.join([
            f'{"Sell It":<10s}|',

            f'{i:>4d}',
            f'{dt.date().isoformat()}',
            
            f'{self.pos:>2d}',
            f'{self.nbar:>2d}',
            
            f'{sprice:.2f}', 
            f'{self.sl:.2f}',
            f'{sroc1:>7.2%}',
            f'{closed_pnl:>7.2f}', 
            '\n',
            ]))
        return self.pos
   

    def register(self, i, dt, row, df, sl_ref='trend'):
        self.nbar += 1
        if sl_ref=='trend':
            self.sl = row.trend
        elif sl_ref=='near_low':
            self.sl = row.llv
        elif sl_ref=='low':
            self.sl = row.low
        elif sl_ref=='row_ema':
            self.sl = row.ema
        elif sl_ref=='mml':
            self.sl = row.mml + self.nbar * row.bwidth/self.p['upslope']
        elif sl_ref=='mmh':
            # self.sl = row.sl
            self.sl = row.mmh

        self.sl_l.append((dt, self.sl))
        self.pos_l.append((dt, self.pos))
        self.horizon.append((dt, self.hline))
        self.sroc1_l.append((dt, row.roc1))
        print(' '.join([
            f'{"TakePains":<10s}|', 

            f'{i:>4d}',
            f'{dt.date().isoformat()}',
            
            f'{self.pos:>2d}',
            f'{self.nbar:>2d}',
            
            f'{row.close:.2f}',
            f'{self.sl:.2f}',
            f'{row.roc1:>7.2%}',
        ]))

    
    def exit_by_nbar(self, i,dt,row,df, nbar=8):
        self.pos=0; 
        sprice = row.close
        sroc1 = sprice/row.close1 - 1 - self.comm
        self.sroc1_l.append((dt, sroc1))
        self.nbar += 1
        
        closed_pnl = (sprice/self.bprice - 1)*100
        self.pos_l.append((dt, self.pos))
        print(' '.join([
            f'{"Exit nbar":<10s}|',
            
            f'{i:>4d}',
            f'{dt.date().isoformat()}',
            
            f'{self.pos:>2d}',
            f'{self.nbar:>2d}',
                        
            f'{sprice:.2f}', 
            f'{self.sl:.2f}',
            f'{sroc1:>7.2%}',
            f'{closed_pnl:>7.2f}', 
            '\n',
            ]))
        return self.pos

    def moveup(self, i, dt, row, df, sl_ref='trend'):
        # new_sl = df.low[i-self.p['n_tsl'] : i+1].min()
        
        if sl_ref=='near_low':
            new_sl = row.llv
        elif sl_ref=='trend':
            new_sl = row.trend
        
        self.sl = max(self.sl, new_sl); 
        self.sl_l.append((dt, self.sl))
        
        self.pos_l.append((dt, self.pos)) # 头寸不变, 登记头寸记录
        
        self.horizon.append((dt, self.hline))
        self.nbar += 1
        self.sroc1_l.append((dt, row.roc1))
        print(' '.join([
            f'{"MoveUp Kp":<10s}|',
            
            f'{i:>4d}',
            f'{dt.date().isoformat()}',
            
            f'{self.pos:>2d}',
            f'{self.nbar:>2d}',
            
            f'{row.close:.2f}', 
            f'{self.sl:.2f}',
            f'{row.roc1:>7.2%}',
            ]))


### Signal class
class Signal:
    ''' 
    定义交易信号类, 为下单指令提供依据, 
    回测时打印该消息, 可策略制定提供参考
    属性有:
        value: 为仓位pos提供依据
            1: 可以全仓持有
            0.5 可以半仓持有:
                if not pos: set pos=0.5
                if pos: set pos=0.5
            1/3: 可以持有三成仓
                if not pos: set it to 1/3
                if pos: set pos=1/3
            0:  set pos = 0 anyway
        message: str, 提示信息, 比如
            看多依据: 大阳线 k线上位(位于均线之上)
            看空依据: 大阴线 k线下位
            还可以加上量化的指标值, 比如roc的幅度等等
        wave: 波段性质, int
            1: 主升浪
            -1: 主跌浪
            0: 横盘走势
            
            
    example:
        s=Signal(1, '均线上位', 1, warn='下周留意逢高派发')
        print(s)            
    '''

    wave_dct = {
        0: '横盘走势',
        1: '主升浪', 
        -1: '主跌浪',
        }
    def __init__(self, value=0, message='', wave=0, **kwargs):
        self.value=value
        self.message=message
        self.wave=self.wave_dct[wave]
        for k,v in kwargs.items():
            setattr(self, k, v)

    def __str__(self):
        lst = list(map(lambda x: (x, getattr(self,x)), vars(self)))
        return f'交易信号:{str(lst)}'
        

